import rita.*;

import controlP5.*;
import sojamo.drop.*;

SDrop drop;

ControlP5 cp5;
Textarea myTextarea;

int MAX_LINE_LENGTH = 60;

String currentStrings[] =new String[0];

RiMarkov markov;

void setup()
{    
  size(350, 400 );

  smooth();
  drop = new SDrop(this);

  cp5 = new ControlP5(this);

  myTextarea = cp5.addTextarea("txt")
    .setPosition(10, 60)
      .setSize(330, 330)
        .setFont(createFont("Serif", 15))
          .setLineHeight(16)
            .setColor(color(0))
              .setColorBackground(color(255, 100))
                .setColorForeground(color(255, 100));
  ;
  myTextarea.setText(
  "semi functional textgenerator\n\n"+
  "how to:\n"+
  "1 drag a .txt file or some\n   clipboard text unto this window"+
 "\n2 pres generate as many\n   times you want"+
"\n3 write a file name to\t save to\n   example:\"doom\" which results\n   in a file called doom.txt in\n   this programs folder/data"
  );

  cp5.addTextfield("saveName")
    .setPosition(60, 10)
      .setSize(230, 40)
        .setFont(createFont("arial", 20))
          .setAutoClear(false)
            ;
  cp5.addBang("generate")
    .setPosition(10, 10)
      .setSize(40, 40)
        .getCaptionLabel().align(ControlP5.CENTER, ControlP5.CENTER)
          ;   
  cp5.addBang("save")
    .setPosition(300, 10)
      .setSize(40, 40)
        .getCaptionLabel().align(ControlP5.CENTER, ControlP5.CENTER)
          ;   

  // create a new markov model w' n=3
}

void draw()
{
  //background(255);
}
void loadFile(String path) {
  String lines[] = loadStrings(path);
  markov = new RiMarkov(this, 2);  
  markov.setTokenizerRegex("\\s");
  markov.setMinSentenceLength(3);
  markov.loadFile(path);
      myTextarea.setText(join(lines,"\n"));

}

public void save() {
  //saveThis();
  saveThis(cp5.get(Textfield.class, "saveName").getText());
}

void generate() {
  currentStrings=markov.generateSentences(10);
  myTextarea.setText(join(currentStrings, "\n"));
}

void saveThis(String fileName) {
  saveStrings(sketchPath("")+"data/"+fileName+".txt", currentStrings);
}

void loadText(String texts) {
  markov = new RiMarkov(this, 2);  
  markov.setTokenizerRegex("\\s");
  markov.setMinSentenceLength(3);
//markov.disableSentenceProcessing();

  markov.loadText(texts);
    myTextarea.setText(texts);

}

void dropEvent(DropEvent theDropEvent) {

  if (theDropEvent.isFile()) {
    println(theDropEvent.file().getPath());
    loadFile(theDropEvent.file().getPath());
  }
  else if (!theDropEvent.isImage() && !theDropEvent.isURL()) {
    loadText(theDropEvent.toString());
  }
}

